﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using WpfApp1.MVVM.Model;
using System.IO;
using Newtonsoft.Json;

namespace WpfApp1.Services
{
    public class FileIOService<T>
    {
        private string PATH;
       
        public FileIOService(string path)
        {
            PATH = path;
        }

        public ObservableCollection<T> LoadCostCategoriesCollection()
        {
            var FileExist = File.Exists(PATH);
            if (!FileExist)
            {
                File.CreateText(PATH).Dispose();
                return new ObservableCollection<T>();
            }
            using (var reader = File.OpenText(PATH))
            {
                var fileText = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<ObservableCollection<T>>(fileText);
            }
        }

        public void SaveCostCategoriesCollection(object _costCategories)
        {
            using (StreamWriter writer = File.CreateText(PATH))
            {
                string output = JsonConvert.SerializeObject(_costCategories);
                writer.Write(output);
            }
        }
    }
}
