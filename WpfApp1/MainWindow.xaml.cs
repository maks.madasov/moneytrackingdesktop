﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1.Services;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = new MainWindowViewModel();
            InitializeComponent();
        }
        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            RadioButton selectedRadio = (RadioButton)e.Source;
           
            if (selectedRadio.Content.ToString() == "Категории затрат")
            {
                CView.Visibility = Visibility.Hidden;
                AView.Visibility = Visibility.Hidden;
                CCView.Visibility = Visibility.Visible;
            }
            else if (selectedRadio.Content.ToString() == "Расходы")
            {
                CCView.Visibility = Visibility.Hidden;
                AView.Visibility = Visibility.Hidden;
                CView.Visibility = Visibility.Visible;
            }
            else if (selectedRadio.Content.ToString() == "Аналитика")
            {
                CView.Visibility = Visibility.Hidden;
                CCView.Visibility = Visibility.Hidden;
                AView.Visibility = Visibility.Visible;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CView.Visibility = Visibility.Hidden;
            AView.Visibility = Visibility.Visible;
            CCView.Visibility = Visibility.Hidden;
        }
    }
}
