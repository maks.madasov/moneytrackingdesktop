﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using WpfApp1.MVVM.Model;
using WpfApp1.Implementations;
using System.Windows.Input;
using WpfApp1.Services;
using System.Windows;

namespace WpfApp1.MVVM.ViewModel
{
    class CostCategoriesModelView : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public static ObservableCollection<CostCategories> CostCategoriesCollection { get; set; } = new ObservableCollection<CostCategories>();
        public ICommand AddCostCategories { get; set; }
        public ICommand RemoveCostCategories { get; set; }
        private FileIOService<CostCategories> _fileIOService;
        private readonly string PATH = $"{Environment.CurrentDirectory}\\costCategories.json";

        private CostCategories _CostCategories;
        public CostCategories SelectedCostCategories
        {
            get => _CostCategories;
            set
            {
                _CostCategories = value;
                NotifyPropertyChanged();
            }
        }

        private string _NewCostCategories;
        public string NewCostCategories
        {
            get => _NewCostCategories;
            set 
            {
                _NewCostCategories = value;
                NotifyPropertyChanged();
            }
        }


        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public CostCategoriesModelView()
        {
            _fileIOService = new FileIOService<CostCategories>(PATH);

            try
            {
                CostCategoriesCollection = _fileIOService.LoadCostCategoriesCollection();
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            //CostCategoriesCollection.Add(new CostCategories() { Name = "Продукты" });
            //CostCategoriesCollection.Add(new CostCategories() { Name = "Квартира" });
            //CostCategoriesCollection.Add(new CostCategories() { Name = "Машина" });

            AddCostCategories = new CommandHandler()
            {
                Method = AddMethod,
                CanExecuteMethod = CanAdd
            };

            RemoveCostCategories = new CommandHandler()
            {
                Method = RemoveMethod,
                CanExecuteMethod = CanRemove
            };
        }

        public void AddMethod(object parameter)
        {
            CostCategories costCategories = new CostCategories()
            {
                Name = NewCostCategories
            };
            CostCategoriesCollection.Add(costCategories);

            SaveData();
            NewCostCategories = string.Empty;
        }

        public bool CanAdd(object parameter) => !string.IsNullOrEmpty(NewCostCategories);

        public void RemoveMethod(object parameter)
        {
            CostCategoriesCollection.Remove(SelectedCostCategories);
            SaveData();
        }

        public bool CanRemove(object parameter) => SelectedCostCategories != null;

        private void SaveData()
        {
            try
            {
                _fileIOService.SaveCostCategoriesCollection(CostCategoriesCollection);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
