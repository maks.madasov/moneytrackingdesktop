﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using WpfApp1.MVVM.Model;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Collections.Specialized;

namespace WpfApp1.MVVM.ViewModel
{
    public class AnalizeModelView
    {
        public ObservableCollection<CostCategories> CostCategoriesHandler { get; set; }
        public ObservableCollection<Cost> CostHandler { get; set; }
        static public Dictionary<string, int> CostDictionary { get; set; } = new Dictionary<string, int>();

        public AnalizeModelView(ObservableCollection<CostCategories> CCH, ObservableCollection<Cost> CH)
        {
            CostCategoriesHandler = CCH;
            CostHandler = CH;

            foreach(CostCategories tmp in CostCategoriesHandler)
            {
                CostDictionary.Add(tmp.Name, 0);
            }

            foreach (Cost tmp in CostHandler)
            {
                foreach (CostCategories tmp2 in CostCategoriesHandler)
                {
                    if (tmp.Categories == tmp2.Name)
                    {
                        CostDictionary[tmp.Categories] += int.Parse(tmp.VCost);
                    }
                }
            }

            CostHandler.CollectionChanged += c_CollectionChanged;
            CostCategoriesHandler.CollectionChanged += c_CollectionChanged;
        }

        public void Update()
        {
            CostDictionary.Clear();
            foreach (CostCategories tmp in CostCategoriesHandler)
            {
                CostDictionary.Add(tmp.Name, 0);
            }
            foreach (Cost tmp in CostHandler)
            {
                foreach (CostCategories tmp2 in CostCategoriesHandler)
                {
                    if (tmp.Categories == tmp2.Name)
                    {
                        CostDictionary[tmp.Categories] += int.Parse(tmp.VCost);
                    }
                }
            }
        }

        void c_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Update();
        }
    }
}
