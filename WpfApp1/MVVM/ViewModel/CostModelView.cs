﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp1.MVVM.Model;
using WpfApp1.Implementations;
using WpfApp1.MVVM.View;
using System.Windows;
using WpfApp1.Services;

namespace WpfApp1.MVVM.ViewModel
{
    class CostModelView : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public static ObservableCollection<Cost> CostCollection { get; set; } = new ObservableCollection<Cost>();
        public ObservableCollection<CostCategories> Handler { get; set; } 
        public ICommand AddCost { get; set; }
        public ICommand RemoveCost { get; set; }

        private FileIOService<Cost> _fileIOService;
        private readonly string PATH = $"{Environment.CurrentDirectory}\\cost.json";

        private Cost _Cost;
        public Cost SelectedCost
        {
            get => _Cost;
            set
            {
                _Cost = value;
                NotifyPropertyChanged();
            }
        }

        private string _NewCost;
        public string NewCost
        {
            get => _NewCost;
            set
            {
                _NewCost = value;
                NotifyPropertyChanged();
            }
        }

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public CostModelView()
        {
            _fileIOService = new FileIOService<Cost>(PATH);

            try
            {
                CostCollection = _fileIOService.LoadCostCategoriesCollection();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            //CostCollection.Add(new Cost() { Categories = "Продукты", VCost = "100", Comment = "Лента", Date=new DateTime(2022,01,01) }) ;
            //CostCollection.Add(new Cost() { Categories = "Квартира", VCost = "200", Comment = "Аренда", Date = new DateTime(2022, 01, 02) });
            //CostCollection.Add(new Cost() { Categories = "Машина", VCost = "300", Comment = "Бензин", Date = new DateTime(2022, 01, 03) });

            AddCost = new CommandHandler()
            {
                Method = AddMethod,
                CanExecuteMethod = CanAdd
            };

            RemoveCost = new CommandHandler()
            {
                Method = RemoveMethod,
                CanExecuteMethod = CanRemove
            };
        }

        public void AddMethod(object parameter)
        {
            AddCostView addCostView = new AddCostView();
            addCostView.Handler = Handler;
            if (addCostView.ShowDialog() == true)
            {
                Cost cost = new Cost()
                {
                    Categories = addCostView.NewCostModal.Categories,
                    VCost = addCostView.NewCostModal.VCost,
                    Comment = addCostView.NewCostModal.Comment,
                    Date = addCostView.NewCostModal.Date
                };
                CostCollection.Add(cost);
                SaveData();
            }
        }

        public bool CanAdd(object parameter) => true;

        public void RemoveMethod(object parameter)
        {
            CostCollection.Remove(SelectedCost);
            SaveData();
        }

        public bool CanRemove(object parameter) => SelectedCost != null;

        private void SaveData()
        {
            try
            {
                _fileIOService.SaveCostCategoriesCollection(CostCollection);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
