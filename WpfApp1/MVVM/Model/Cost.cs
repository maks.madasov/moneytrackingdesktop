﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.MVVM.Model
{
    public class Cost
    {
        public string Categories { get; set; }
        public string VCost { get; set; }
        public string Comment { get; set; }
        public DateTime Date { get; set; }
        public override string ToString() => $"{Categories} - {VCost} - {Comment} - {Date}";

    }
}
