﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using WpfApp1.MVVM.Model;
using WpfApp1.MVVM.ViewModel;
using System.Collections.Specialized;

namespace WpfApp1.MVVM.View
{
    /// <summary>
    /// Логика взаимодействия для AnalizeView.xaml
    /// </summary>
    public partial class AnalizeView : UserControl
    {
        public Dictionary<string, int> CostDictionary = AnalizeModelView.CostDictionary;
        private int AllSum = 0;
        private List<SolidColorBrush> colors { get; set; }
        public AnalizeView()
        {
            InitializeComponent();

            colors = new List<SolidColorBrush>()
            {
                new SolidColorBrush() {Color = Color.FromRgb(245,134,133)},
                new SolidColorBrush() {Color = Color.FromRgb(236,80,81)},
                new SolidColorBrush() {Color = Color.FromRgb(247,140,184)},
                new SolidColorBrush() {Color = Color.FromRgb(223,159,23)},
                new SolidColorBrush() {Color = Color.FromRgb(123,135,157)},
                new SolidColorBrush() {Color = Color.FromRgb(119,186,230)},
                new SolidColorBrush() {Color = Color.FromRgb(186,167,195)},
                new SolidColorBrush() {Color = Color.FromRgb(234,181,113)},
                new SolidColorBrush() {Color = Color.FromRgb(176,113,202)},
                new SolidColorBrush() {Color = Color.FromRgb(222,186,66)}
            };
        }
        
        private void SetUp()
        {
            listbox.Items.Clear();
            canv.Children.Clear();

            int si = 0;
            foreach (var i in CostDictionary)
            {
                StackPanel sp = new StackPanel()
                {
                    Orientation = Orientation.Horizontal
                };
                Rectangle color = new Rectangle()
                {
                    Height = 7,
                    Width = 7,
                    Fill = colors[si]
                };
                TextBlock name = new TextBlock() { Text = i.Key, Width = 350, Margin = new Thickness(4) };
                TextBlock summ = new TextBlock() { Text = i.Value.ToString(), Width = 100, Margin = new Thickness(4) };
                double tmp = ((double)i.Value / (double)AllSum) * 100;
                tmp = Math.Round(tmp);
                TextBlock perc = new TextBlock() { Text = tmp.ToString() + "%", Width = 50, Margin = new Thickness(4) };

                int wid = (int)canv.ActualWidth;
                Rectangle rect = new Rectangle()
                {
                    Height = 40,
                    Width = wid * tmp / 100,
                    Fill = colors[si],

                };
                //Canvas.SetLeft(rect, 0 + si * 40);
                canv.Children.Add(rect);

                sp.Children.Add(color);
                sp.Children.Add(name);
                sp.Children.Add(summ);
                sp.Children.Add(perc);
                listbox.Items.Add(sp);
                si++;
            }
            
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            CostModelView.CostCollection.CollectionChanged += ca_CollectionChanged;
            CostCategoriesModelView.CostCategoriesCollection.CollectionChanged += ca_CollectionChanged;
            AllSum = 0;
            foreach (var i in CostDictionary)
            {
                AllSum += i.Value;
            }
            SetUp();
        }

        void ca_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            AllSum = 0;
            foreach (var i in CostDictionary)
            {
                AllSum += i.Value;
            }
            SetUp();
        }
    }
}
