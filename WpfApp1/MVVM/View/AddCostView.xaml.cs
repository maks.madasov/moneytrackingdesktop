﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.MVVM.Model;
using WpfApp1;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace WpfApp1.MVVM.View
{
    /// <summary>
    /// Логика взаимодействия для AddCostView.xaml
    /// </summary>
    public partial class AddCostView : Window
    {
        public ObservableCollection<CostCategories> Handler { get; set; }
        public DateTime dateDefault { get; set; } = DateTime.Now;
        public AddCostView()
        {
            _NewCostModal = new Cost(); 
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _NewCostModal.Categories = cbCategories.Text.ToString();
            _NewCostModal.VCost = tbCost.Text.ToString();
            _NewCostModal.Comment = tbComment.Text.ToString();
            _NewCostModal.Date = (DateTime)date.SelectedDate;
            this.DialogResult = true;
        }

        private Cost _NewCostModal;
        public Cost NewCostModal 
        {
            get => _NewCostModal;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cbCategories.ItemsSource = Handler;
            cbCategories.SelectedItem = Handler.First();
            date.SelectedDate = dateDefault;
        }

        private void tbCost_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
