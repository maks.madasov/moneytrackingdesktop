﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WpfApp1.MVVM.ViewModel;

namespace WpfApp1
{
    class MainWindowViewModel
    {
        public CostCategoriesModelView CostCategoriesCollection { get; set; }
        public CostModelView CostCollection { get; set; }
        public AnalizeModelView Analize { get; set; }

        public MainWindowViewModel()
        {
            CostCategoriesCollection = new CostCategoriesModelView();
            CostCollection = new CostModelView();
            Analize = new AnalizeModelView(CostCategoriesModelView.CostCategoriesCollection, CostModelView.CostCollection);
            CostCollection.Handler = CostCategoriesModelView.CostCategoriesCollection;
        }
    }
}
